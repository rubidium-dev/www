---
title: About
permalink: /about/
---

# So who am I?

And why did I make this site? Well, you can think of me as consisting of a jumbled pile of labels:

* human ...
* ... but also a cabbit/dragon
* trans-feminine
* techie
* asexual
* writer

That’s the easiest way I can sum things up. I created this blog so I could post about whatever I feel like. Some days
that might be programming in Rust, some days it might be LGBT-related musings about life. This is my space to do what
I want. If you’re only interested in the programming stuff, then you might want to follow my
[GitLab page](https://gitlab.com/rubidium-dev). I also have a
[Twitter feed](https://twitter.com/RustedRedCabbit) if you want art and completely random off-the-cuff thoughts.

## Who is Ruby?

<figure class="right">
<img src="/assets/images/ruby-snowmishibari.png" width="300" height="616" alt="Ruby, a humanoid cabbit/dragon hybrid with white fur and red scales.">
<figcaption>Artwork © <a href="https://www.furaffinity.net/view/31375403/">Snow Mishibari</a></figcaption>
</figure>

Rubilah “Ruby” Rubidium is both my online alias and my [fursona](https://www.merriam-webster.com/words-at-play/history-of-furry-and-fursona).
She is a female-identifying transgender [cabbit](https://en.wikipedia.org/wiki/Cabbit)/dragon hybrid, and she has a predominantly
rabbit body type with feline face, dragon tail, and scales on her belly. She has a large marquise-cut ruby embedded in her forehead, part of
her cabbit heritage. Supposedly the gem should impart magical powers, but Ruby has never exhibited any such abilities. She has long, wavy
blonde hair that is shaved on the right side. She enjoys wearing dresses and long, flowing blouses so that her legs and tail can be free.

Ruby is generally relaxed and easygoing, but she can be stubborn and opinionated about certain things that she’s passionate about. She's
also not a morning person by any stretch of the imagination and can be quite moody until she’s had her second cup of coffee. She enjoys
creative pursuits like writing and computer programming, and she relaxes by listening to music and playing video games. She's very much
an introvert and doesn’t enjoy loud groups of people, but she loves the company of her closest friends.

**Likes:** Rain, warm blankets, books<br>
**Dislikes:** Noisy people and places, bragging<br>
**Birth date:** November 5<br>
**Blood type:** B+<br>
**Favorite foods:** Sushi (especially _unagi_), spaghetti, coffee<br>
**Favorite games:** City of Heroes, The Legend of Zelda: Breath of the Wild<br>
**Favorite music:** Folk, classic rock<br>
**Sexual preferences:** asexual, bi-romantic<br>
**Zodiac:** Scorpio (Western), Goat (Eastern)

## That's a Lot of Nonsense About an Imaginary Rabbit-person
Why is this so important? Well, my fursona is me. It’s how I think about myself and how I project myself on the Internet. It’s a **mask**
I wear, for certain, but it’s also a reflection of who I truly am. Ruby is a complex character, and I think she reflects the duality
present in my own personality. Her cabbit half is soft and cuddly and not at all threatening, kind of like how I am at home and with
friends. Her dragon half is fierce and confident, a symbol of my professional life and my personal ambition.

I’ve had a couple previous fursonas (fursonae?), a plain ol’ rabbit and fox. With Ruby, I tried something different and spent a lot of
time picking through different visual references to put something together that reflected conscious choices for every single aspect of
her design rather than just copying a specific animal/breed wholesale. You can learn more about her character design in
[this post](/2019/05/18/ruby-redesigning-a-fursona.html).

The funny thing is, I avoided doing this in the past specifically because I didn’t want to be labelled one of “those furries” (whatever
that means). When I got re-involved in the fandom around 2008, the people I hung out with had a lot of opinions about what they considered
“typical furries”—i.e. they ridiculed things like rainbow-colored fur, hybrids, wings, that kind of stuff. There was even silly prejudices
against various species: all foxes were sluts, all dragons were assholes, etc. To be fair, it wasn’t at all mean-spirited, it’s just one
of those phenomena I’ve witnessed in many subcultures. “What do you mean? We’re nerds but we’re not as nerdy as those nerds over there!”

That peer pressure influenced how I expressed myself for a long time. I didn’t want to violate the established social norms of my chosen
friends. After drifting apart from that group over time, though, I’ve slowly evolved past that thinking and feel a bit more comfortable
choosing a new personality of my very own to wear without outside influence.