---
title: Ruby's devblog
---
# Latest Posts

<div class="latest">
{% for post in site.posts limit:5 %}
<div class="latest_post">
<h2><a href="{{ post.url}}">{{ post.title }}</a></h2>
<h4>posted {{ post.date | date_to_string }} •
    {% for tag in post.tags %}
    <a href="/tag/{{ tag | slugify }}.html">{{ tag }}</a>{% if tag != post.tags.last %},{% endif %}
    {% endfor %}
</h4>

{{ post.excerpt }}

{% if post.content contains site.excerpt_separator %}
<p><a href="{{ post.url }}">Read more...</a></p>
{% endif %}
</div>
{% endfor %}
</div>

<p><a href="/archive/">More posts ...</a></p>