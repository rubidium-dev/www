---
title: My thoughts on Epic store exclusivity
date: 2019-04-03 19:00 -0500
tags: ["epic","games","steam"]
---
I debated whether or not I even wanted to weigh in on this topic, as it's charged with a lot of emotions amongst the gaming community
right now, and I'm not really looking forward to attracting controversy. That said, I do have some varying thoughts on the subject and
wanted to offer my personal take on the issue. A good portion of the responses I'm seeing to this subject are very emotional and
reactionary, and it is perfectly valid to feel upset, but I wanted to share some of the different thoughts I've had in rationalizing
and examining the situation.

Of course, I play games so I have the same perspective as a lot of gamers as a consumer and how this affects my personal choices when
buying PC games. You probably won't see anything new there if you, too, purchase games and play them. But I also bring a slightly
different perspective from most of the reactions I've witnessed, in that I'm actually a game developer by trade (in a variety of spaces,
including AAA console/PC, mobile, and others). I'm not an executive so I don't have the full business perspective, but I've sat in enough rooms
with publishers and platform holders that I have some exposure to how some of those deals go down.

<!--more-->
Also, I'm not arguing for or against the practice of store/platform exclusives. You're probably an adult, and as a purchaser of games you're
free to spend your money how and where you like. I'm not interested in telling you what you should do. I'm also not going to get
into the whole conversation around whether or not [Epic's launcher is spyware](https://www.pcgamesn.com/epic-launcher-spyware), as
that's a wholly different issue. I'm just here to talk about the economics of platform exclusivity.

## My personal opinion
As a consumer, I'm not really invested into any one platform. I have a large library (almost 350 games) on Steam, but I also have
purchases on GOG, Origin, and others. For one-time purchases, I don't really care that much where I buy a game digitally anymore
than I do when buying one physically since it's the same game regardless of where it came from (for the most part—retailer exclusives
are a related but separate thing I don't want to get into right now). Because of that, Epic store exclusives don't really
factor into my purchasing decision, other than the possible momentary inconvenience of registering a new account, which really is
a small inconvenience since I use a password manager. Multiple launchers also doesn't bother me a whole lot because I prefer not
to leave them running all the time; so it's the same amount of effort to start Steam as it is Origin, for instance.

Essentially I don't have any real emotional response to the Epic store having exclusivity agreements. I do find it interesting that
this is a problem specifically for the Steam crowd; console players have been dealing with platform exclusives for decades, so
maybe they just accepted their fate long ago? There really hasn't been a credible competitor to Steam since it's inception.
Certainly there are _alternatives_, but none that I would say is a real threat to Steam's insurmountable market share. At least,
not until Epic came along.

## Why developers are jumping ship to the Epic store
It's not really any secret; it's all about the money. I think sometimes gamers forget this, but despite what you may think about
the passions of any individual developers involved in a title, unless they're indie they all ultimately answer to some form of
corporate overlord. And corporations are there to do one thing: make profit. Any decisions made are less about morals, ethics,
or what's good for games and more about balancing how much sales will increase versus how much they will decrease based on any
given decision. That may sound cynical, but that's just capitalism. It's why I work for a paycheck and not the "privilege" of
working on games. While I love video games, only one of those things pays the rent.

In this case, it's enough money that developers and publishers are willing to take risks in alienating the hardcore Steam audience for
an unestablished storefront. Businesses don't like risk unless there's enough of a payoff. Epic has an audience, no doubt—just
look at the wild success of _Fortnite_. But it has yet to prove that they can mobilize that audience on to other titles on their
store, so I wouldn't say these companies are betting on that. No, it really comes down to two very appealing piles of cash.

The first one is the obviously far friendlier revenue share that Epic offers, namely 88% to the developer/publisher and 12% to
Epic. Steam has used the long-establish 70/30 split (which I believe was popularized by Apple's iTune services, but may have
a longer history somewhere). Developers have long accepted this situation mostly because Steam was the only game in town. Only
the biggest players like EA and Blizzard sold direct-to-consumer to avoid having to share their revenue with Steam. Even Steam's
terms are better than physical retail, which typically see the developer/publisher only getting about 50% of the MSRP, and you
have to deduct all the manufacturing costs of the physical boxes/media. But digital is the future and is pushing these terms
even further in the favor of the devs rather than the retailer. Another huge factor in this point is that if you use the Unreal
Engine (as is the case with the recently announced _Borderlands 3_), you don't have to pay Epic the usual licensing cut for the
engine, which is another 5%.

The second set of money on the line is the one that gamers are taking exception to, and that's the platform exclusivity. While
the higher developer cut would attract more games on its own, this in and of itself would just result in devs selling their
games on Epic _and_ Steam. The end result is that most likely people would just continue to buy their games on Steam, and the
Epic store's customer base would only grow at a trickle. Such is how monoplies work; to a certain degree, they're counting on
customer inertia to maintain their market share. So exclusivity is a way to draw people to the platform. This may sound
anti-competitive (you could certainly make that argument), but to be honest there's almost no other way to compete against a
behemoth like Steam. (Most) established customers on a platform like that are just not going to defect on promises of better
features, customer service, or the like. Thus, Epic isn't even trying to compete in that way at the moment. (I assume they'll make good on their word and have comparable or better features at _some_ point—it's just not an early priority.)

That said, Epic is creating a differential on one of Steam's biggest weaknesses: signal-to-noise ratio. Steam is as close to
a "free market" as you're going to see. As long as it's not outright illegal, a developer can probably get their game on Steam.
While this is incredibly egalitarian, it also means there are thousands of titles that quite frankly just aren't worth buying,
and their discovery tools, while getting better, aren't developed enough to help people find games they might truly be
interested in. (Compare to a marketplace like Amazon, where they have an order of magnitude more products across a much
wider set of genres, and they're infinitely better at this.) Epic is adopting a curated approach, and I think that will help
differentiate them from Steam in the long run. Unfortunately, again, it doesn't translate directly into sales.

Now, on the surface this wouldn't seem like an advantage for the developer or publisher, since only selling on one storefront
isn't going to reach as many players, right? But the advantage here is in the assistance that comes with exclusivity. In today's
market, it isn't enough to just hope that your game is good (or even that it has a recongizable brand). Whether you're AAA or
indie, you need to have a marketing budget (there are a few standout examples that didn't need a huge marketing budget, but
those are very much exceptions to the rule). Marketing and advertising isn't cheap, and platform holders usually promise pretty
significant sums of cash (for AAA, this can be millions of dollars for the biggest, most desirable titles). Additionally, as
an exclusive you usually get white glove treatment from the platform holder, not just for integration, but also completely new
features on the platform that your game might need.

Sometimes, these platform exclusivity deals also include a minimum guaranteed number of sales. In other words, if the game
doesn't sell a predetermined number of copies on the platform, the platform holder might pay the difference, or the exclusivity
contract might terminate earlier than it would otherwise. This is pretty rare though.

## Why this is bad
Obviously, this is frustrating for some consumers who are invested in a platform. Your games library and your friends are all on
Steam, and having to move over to Epic to play that one game you wanted is a huge hassle. This is something devs have to weigh; is
the extra support from Epic worth the exclusivity? Only time (and actual sales numbers) will be able to tell. In the past, it
has been worth it plenty of times for console titles. But this is still somewhat new territory for PC games outside of the
biggest publishers.

In an ideal world, you'd have the choice of buying your game from wherever you want, and multiplayer cross-play would be enabled
for every title so you if some of your friends buy on Steam and some buy on Epic, you can still play together. We're starting to
see some of this movement in the console realm, but even there the biggest platform holder (Sony) is trying to maintain its market
share by _not_ playing along except when it feels it has something to lose (like _Fortnite_ players).

From a dev's perspective, Epic is also an untested platform. They've done well for themselves with their own games, but they don't
have a super long track record when it comes to selling and supporting other developer's titles. It could be by the end of this year we
see Epic's platform exclusivity deals backfire spectacularly. But I think the titles they've chosen so far are popular enough that
that won't be the case. I think at worst a few publishers decide it wasn't worth it and just don't do it again, but we're not
likely to see any flops.

## Why this is good
To be perfectly honest, Steam has needed a good kick in the pants for a long time. Monoplies tend to stop innovating or providing
good service to their customers, because without meaningful competition they have no reason to. They'll do just the minimum amount
of work to ensure they don't lose significant amounts of customers. The _laissez-faire_ style platform
is evidence of this, as it's not particularly friendly to players, and it doesn't encourage good dev behavior (just look at all
the asset flips and similar). It's only good for Steam because even making a handful of dollars on a crappy game is just more money
in the bank for them.

Valve has been making some strides in recent memory, such as instituting a refund policy (which they probably had to as a direct
result of all the crapware) and making [a half-hearted effort to improve their revenue share with developers](https://steamcommunity.com/groups/steamworks#announcements/detail/1697191267930157838).
But even though you'll notice they only extended this olive branch to those developers who bring in the most money. That's how
they've operated for many years now—if you're not in the top percentage of earners on the platform, you're not likely to get any
direct assistance from them. I've experienced this firsthand. Valve's staff can sometimes be difficult to work with even to some
of us in the AAA space, and [we sometimes just have to work around deficiencies in their platform](https://www.fortressofdoors.com/operation-tell-valve-all-the-things-3-0/).

Unfortunately, this means a lot more platform exclusives in the short term. As I said earlier, Epic has no hope of releasing Steam's
stranglehold on the market without tactics like this. My hope is that they can tilt things in their favor quickly enough that
it isn't necessary in perpetuity. I think, long term, they could put their own games on the platform as exclusives if that's what
they want to do, and third parties will see value in releasing on multiple storefronts so that players have choice and there's
enough healthy competition in the industry that Epic and Valve will try to differentiate their storefronts in other ways. We just have
to expect the market to go through some growing pains in the near future.