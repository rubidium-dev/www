---
layout: post
title: 'Ruby 2.0: Redesigning a fursona'
date: 2019-05-18 10:48 -0500
card: /assets/images/2019/05-18/another-ruby.jpg
tags: ["art","commissions","furry","fursona","personal","trans"]
---
**Warning: Post contains some pictures that might be considered NSFW (Barbie-doll nudity).**

----
[Fursonas](http://en.wikifur.com/wiki/Fursona) are a fascinating subject to me, mostly as an intersection of identity expression and roleplaying. For
some, a fursona is simply a character, a doll to dress up and use to play out scenes in their imagination. For others, it becomes an idealized self,
the ultimate expression of "me but with fur and also how I want to see myself." I imagine most are a spectrum between those two points. The
[[adjective][species] blog](http://www.adjectivespecies.com/2012/09/17/our-fursonas-are-happier-than-we-are/) provides a great exploration of the
fictionalized self through fursonas. It is this aspect in particular that is meaningful to me; most of my fursonas have in some way been an
expression of both who I am and how I want the world to see me.

Two months ago, I decided to redesign my fursona. This is a deeply personal act to me while also being a bit of performance art. As I said, while
my fursona reflects aspects of my inner psyche, they also exaggerate features that I want others to see in me. While I know some furries that have
maintained the exact same fursona for years, sometimes even decades, for me it is very important to keep my self image up to date. Additionally, I
was motivated to change a few things in direct response to engagement with artists, so I have some tips buried in here for other people who might
desire to have complicated character designs.

<!--more-->
## How I got to Rubi 1.0
Recently [I posted on Twitter in response to a meme describing the progression of my fursona](https://twitter.com/RustedRedCabbit/status/1126842671950114818)
in general over the years. Like a lot of furries, I started with some fairly plain fursonas like cats and foxes. Up until 2015, my fursona was always
just "take this animal as-is and anthropomorphize it." Starting out, that was just because the whole thing was new to me, and I didn't yet know all
the ways I could push the boundaries. But even after I figured that out, I hesitated to make my characters any more complex than a typical cartoon
character. At the time, I was hanging around a group of furry friends that were rather judgmental towards other furries. (I've noticed nerd
subcultures are often like this and readily sort themselves into hierarchies.) Having features like wings (on non-avian fursonas), rainbow hair, and
the like was met with derision because you were one of _those_ furries. So in a way that social circle encouraged me to suppress the desire to
explore my characters in more depth because I wanted to ensure that I wasn't a target of ridicule.

As I got older, I realized I didn't enjoy hanging out with those folks very much (they were also toxic in other ways than the above), and I started
to get more mature on my own. I stopped caring as much about what other people thought and made more decisions independent of what critique or
commentary I might receive. So of course one of the things I decided to do when I first designed Ruby (it was originally spelled "Rubi") in 2015,
I kind of went wild.

<figure class="center">
<a href="https://www.furaffinity.net/view/18164834/"><img src="/assets/images/2019/05-18/rubi-1-ref-sheet.jpg" width="700" height="485" alt="Reference sheet showing multiple poses of a cabbit/dragon hybrid anthropmorphic character. She has white fur and blue dragon scales."></a>
<figcaption>The original reference sheet for Rubi's design (2015). Art by <a href="https://twitter.com/artbyetuix">@artbyEtuix</a>.</figcaption>
</figure>

I had secretly always loved hybrid characters, and so I knew from the beginning I wanted to blend some aspects together of different species. She was
mostly rabbit, as I love rabbits in general and have a lot of rabbit collectibles at home. I also brought in the features of a dragon, creatures that
I had a fascination with ever since I first read _The Hobbit_ many, many years ago. The cabbit ("cat/rabbit," a hybrid based on
[Ryo-Ohki](https://en.wikipedia.org/wiki/List_of_Tenchi_Muyo!_characters#Ryo-Ohki) from _Tenchi Muyo!_) was the cherry on top. I liked the idea of a gem
in her forehead, and it felt a natural fit.

Part of the motivation for this complex design was that I wanted to feel _unique_. In a sea of foxes, wolves, cats, and other animals, sometimes it can
be a little hard to stand out without deviating significantly from the beaten path. I imagine that's why you see a lot of otherwise standard animal
fursonas but with crazy color schemes or markings. The other part of this design was simply trying to marry a lot of competing parts of my personality,
based on stereotypical aspects of those creatures. In particular, at work I've been referred to as a "quiet powerhouse," because I tend not to speak
up very often, but when I do it's because it's time for me to get things back on track and get the team going in the right direction. The rabbit
represents that gentle tranquility, and the dragon the willingness to bring up great reserves of power and strength when needed. Including the color
blue heavily in her design represented more of that same tranqulity and passiveness. Ruby was not someone who let her emotions get away from her.

The cabbit/gem angle represents mysticism and spirituality. I'm a deeply spiritual (and mildly religious) person, and I wanted that to be included
in some way, so a magical forehead gem felt like a great shorthand. One of my earlier fursonas actually included open representations of my faith in
the design, but that ended up feeling not quite right.

## Onward to Ruby 2.0
After four years, I had had a lot of time to develop Ruby's persona in my mind and also [commissioned several pieces of art](https://www.furaffinity.net/gallery/rubidagron/folder/387707/Ruby-legacy).
The art in particular taught me a lot about how artists approach my character, and some of the changes to her design are direct responses to that.
However, a lot of it was just an evolution of how I understand myself as a person and a shift in how I relate to the world.

<figure class="center">
<a href="https://www.furaffinity.net/view/30978012/"><img src="/assets/images/2019/05-18/ruby-2-ref-sheet.jpg" width="700" height="484" alt="Reference sheet showing multiple poses of a cabbit/dragon hybrid anthropmorphic character. She white fur and red dragon scales."></a>
<figcaption>The updated reference sheet for Ruby's design (2019). Art by <a href="https://twitter.com/charmrage">@charmrage</a>.</figcaption>
</figure>

While you can tell these are both the same character overall, there are many changes both apparent and subtle. We'll just work our way top down.

**Ruby versus Rubi.** While not a visual change, I'll just get this out of the way. I think I was trying to be cutesy with the "Rubi" spelling back in
2015 (going for even _more_ uniqueness). I decided to change it to a traditional spelling of the name because at some point I started pronouncing it
in my head as "rubby" and coudln't un-hear that.

**The "head crest" is gone.** I'm referring to the three blue fin thingies on her head in the first reference sheet. I think in the end they were just
visual noise. I'm not even sure why I included them originally; I think it might've been copying some features from a _Dungeons & Dragons_ image of a
dragon. It also created some confusion with artists because we couldn't settle on terminology. Sometimes it was a "crest," other times "horns," etc.

**The ears are shorter.** Just a bit of refinement on this one. Her original ears were ridiculously long.

**A new hairstyle with one side shaved.** Her original hairstyle was very "traditional" in its prettiness. I liked it, but this time around I wanted
to express a bit of a rebellious/wild streak. This is typical of me in real life, as I don't want to look unprofessional but sometimes I express
myself with little accents. I kinda want to do my hair like this, but I'm not quite sure yet, so I've hesitated in shaving off half my hair. It's a
fun exploration in art, though!

**She has scales on her spine.** Technically this isn't new, as it was part of the original design. The problem is that Etuix covered up that detail
with her ears and hair in the first reference sheet, so that detail never made it into any subsequent art.

**Hands are completely different.** This was the single biggest inconsistency in all my old art. I tried to make her hands look more like dragon talons
originally, so she has three fingers on each hand instead of four, and they have big claws. A lot of artists just blew past this and drew normal hands
on her. At this point, I decided it wasn't worth continuing to press the issue, and I also wanted to make her a little more humanoid anyways, so the
new design has human-like hands.

**New tail.** Her old tail was smooth with a tuft at the end, feeling kind of cuddly and fluffy. I wanted her new tail to exude strength and a bit
of aggressiveness, so I added spines along this time. It definitely looks more dragon-like (at least what's typical of Western dragons).

**Red scales versus blue.** This is easily the most obvious thematic change. As I mentioned earlier, the first Ruby was colored to express
tranquility and passiveness, where the new is all about power and confidence. Red, the color of passion and fire, seemed like a great choice, and
it was already part of her palette since her gem and eyes were also red.

**Rabbit feet.** This isn't different, but I wanted to call it out, because so many furry artists don't know this little factoid.
[Rabbits _do not_ have paw pads.](/assets/images/2019/05-18/rabbit-feet.jpg). It seems the default that they will draw are either human feet or animal
feet with paw pads. It's important to call this detail out if you have a rabbit character, as the error rate seems to be somewhere in the 80-90%
range from my experience. I made this mistake recently and forgot to call it out ahead of time, and I got another piece of art with Ruby having
pink toes.

**Genitals.** Not pictured here because I don't want NSFW imagery on my blog, but this was actually an important change. Ruby is a trans-feminine
(i.e. male-to-female transsexual) character. She was raised a boy at birth but transitioned into womanhood as an adult, just like me in real life.
The first version of Ruby had a penis because of this, which is not all that uncommon for transgender feminine furry characters. The new version
is equipped with a vagina instead, and there's a specific reason for this. I am about to undergo GRS (gender reassignment surgery, also sometimes
called gender affirmation surgery), so this directly reflects a change in my personal life. It's also what drove me to start this redesign process
in the first place. This is a huge change for me, and exploring it through art has helped me process my anxiety over the procedure. One funny side
effect of this is that I still tag my art as "trans-feminine" on FurAffinity, and people have expressed confusion that she doesn't have a penis.
Trans people come in a lot of different shapes.

## What's in store for the future?
Obviously, I have no idea. But for the first time, I've felt truly attached to my fursona. Shedding my concerns about what other people thought
about her design helped immensely for me to craft a fursona that's both unique and true to who I am. I absolutely love Ruby to death, and I
enjoy commissioning art for her now in a way I haven't for years. I could foresee me portraying Ruby for many, many years to come. That doesn't
mean she won't change again, of course! Maybe next time she'll become a green dragon?

If you want to keep up with her adventures, you can see the art I post over at [FurAffinity](https://www.furaffinity.net/user/rubidagron/). I
also post the SFW stuff on my [Twitter feed](https://twitter.com/RustedRedCabbit).