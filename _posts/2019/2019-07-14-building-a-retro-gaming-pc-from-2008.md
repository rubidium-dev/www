---
layout: post
title: Building a retro gaming PC from 2008
date: 2019-07-14 13:57 -0500
card: /assets/images/2019/07-14/coh-screenshot-1.jpg
tags: ["city of heroes","games","hardware","pc","retro"]
---
One of my hobbies is collecting retro gaming hardware. I have a modest collection of several different consoles and games: Super Nintendo, Sega Genesis,
PlayStation 1/2, Nintendo 64, PlayStation Portable, and Game Boy Advance. Sometimes I just like to kick back with a game from the older generations and
appreciate a bunch of different consoles and games I didn't get the chance to play the first time around.

I mostly gamed on PC through the late 90s and into the 2000s, so it never really occurred to me to revisit older PC hardware. However, my wife is big
into _City of Heroes_, and when
[it recently came back to life]({% post_url /2019/2019-04-21-city-of-heroes-and-the-private-server-kerfuffle-or-why-intellectual-property-is-stupid-and-broken %}),
we started playing together on the [Homecoming servers](https://forums.homecomingservers.com/index.php). One of the interesting things about private
server MMOs is that the client software largely goes unchanged, so theoretically this MMO that shutdown in 2012 should still be playable on some pretty
old hardware.

So I challenged myself to build a decent gaming PC from the mid-2000s that could still run _City of Heroes_ today.

<!--more-->
## Target date: 2004
_City of Heroes_ originally released in April of 2004. For my first attempt at building this PC, I decided to target an average build around the 2004-2005
range. The recommended system requirements for the launch game indicated:

* **CPU:** Pentium 4 or Athlon XP
* **CPU SPEED:** 1.7 GHz
* **RAM:** 512 MB
* **OS:** Microsoft® Windows® XP/Vista/7
* **VIDEO CARD:** 64 MB Direct3D Card (GeForce 5600+/Radeon 9600+)

This seemed easy enough to do and period-appropriate. Right away, though, I knew I didn't want to mess with building the whole thing from the ground up.
Finding all the right components from 15 years ago in good enough shape to still work together was likely going to be a huge challenge. So instead, I
decided to start with a Dell Optiplex chassis as my basis.

Why? Many moons ago, I worked in scrapping and recycling decommissioned computers. The company would buy huge pallets piled with retired computers from
school districts, universities, and local businesses. One thing that always stuck out to me were the Dell Optiplex computers. These things could take
serious beatings and keep working. There were several times where I would get a hold of one that looked like it had fell off the back of a truck and
taken a tumble down a steep hill, and yet it would still boot and run just fine. I don't know much about modern Dell computers, but the Optiplexes from
the early and mid-2000s were built like tanks.

The great thing is these old computers are so plentiful that they're cheap as dirt too. I quickly found a computer listed on Rakuten that fit the bill:
a Dell Optiplex GX620. It was listed as having a 2.8GHz Pentium 4, 1 GB RAM, and a 40 GB HDD, all for about $45. They are heavy, though; the shipping
cost almost as much as the computer. But having a good starting point for less than $100 was still pretty good for my purposes. As an added surprise,
when it arrived it actually contained an 80 GB **SSD** and not a 40 GB HDD, which was pretty cool.

<figure class="center">
<a href="/assets/images/2019/07-14/retropc-mark-1.jpg" target="_blank"><img src="/assets/images/2019/07-14/retropc-mark-1.jpg" width="512" height="384" alt="A Dell Optiplex GX620 tower computer."></a>
<figcaption>Mark I of the retro PC. Sadly he was retired quickly.</figcaption>
</figure>

(It was also listed as having "genuine" Windows XP, but I highly doubt that...)

While I was waiting for it to arrive, I hit up eBay and purchased a few 2004-ish upgrades. Obviously, I would need a 3D graphics card to play
a 3D game. This is where it can get a little tricky with using a business computer base. They tend to have very small power supplies (the one I bought
has a 305W), and no additional PCIe power. You can use something like a SATA-to-PCIe power adapter, but honestly any card that needs the extra power
is probably going to overwhelm the power supply. It's better just to go for a lower tier card that doesn't require extra power. This was fine for my
purposes since I wasn't necessarily trying to build the "best of the best" for the time period. I just wanted a solid playable FPS in _City of Heroes_.
I eventually settled on a GeForce FX 5500, though it wouldn't be my final selection by far. (More on the trials of getting a graphics card later.)

The other pieces I wanted were a 4 GB RAM upgrade and a dedicated sound card. Even though onboard sound even in the mid-2000s was more than adequate,
I really liked the idea of using a Sound Blaster like I used to, so I bought an Audigy 2 ZS. I honestly cannot tell the difference in audio quality
between the onboard sound and the Sound Blaster, but it still feels better to have it. The RAM was a bit more of a challenge. This is where I'll offer
my biggest piece of advice for buying things on eBay: _read everything carefully_. The first batch of RAM I bought was ECC, which is meant
for servers and not supported by the Optiplex motherboard. I double-checked the listing on eBay, and sure enough it was marked as so. I guess I was
just moving too fast and buying things without reading carefully. So that was a bit of a waste. I tried again and made double sure to get the right
_non-ECC_ DDR2 RAM, and we were good to go with 4 GB finally.

Using a 1080p monitor didn't seem very era-appropriate either, so I tracked down an old 1280x1024 flat panel, very similar to what I used to play
_World of Warcraft_ on back when it launched.

Once all the pieces arrived, it didn't take long to get everything put together and ready to go. It took way longer to get Windows XP fully updated.
Sadly, experiment #1 was a failure. The FX 5500 couldn't even bench in [3DMark03](https://benchmarks.ul.com/legacy-benchmarks), chugging along at
0-4 FPS. My research indicated that the FX 5500 should be an okay enough graphics card for the time period, so why it wouldn't even get
going in the benchmark baffled me. Testing it in the game gave the same results; everything was a slide show. The only weird thing about it is
it was listed as "new" and appears to be a recently manufactured aftermarket card, so it's possible they got something wrong from the reference design.

## The epic saga of the graphics card
Thus began probably the biggest waste of time and money in this whole exercise. I had absolutely _the worst luck_ with getting an appropriate, working
graphics card for the machine. Most of this was just the nature of buying parts off of eBay. Some things were mislabeled, some things were just used
and abused to hell and back. I probably should have just checked some local swap meets or used parts shops instead of buying everything online.

Among others, I ended up buying a Geforce 7900 GT that had bad memory and a Radeon HD 6670 with a dead fan. Most of the things I got were too cheap
to really make it worth my while to get in argument with the seller over returns and such. Eventually I landed a Dell OEM Radeon HD 6670 that
actually worked and looked like it hadn't been used much. Now, you'll note that the card is _way_ out of spec for the machine. The 6670 was made
in 2011, not 2004. But at this point I was kind of fed up with buying overpriced dead hardware, so I went for something I knew would work and
didn't look like it had been run over. I probably overpaid a bit.

Sure enough, this finally got me to a working, playable system. The HD 6670 could hit almost 200 FPS in some of the 3DMark03 benchmarks. Mission
accomplished! Let's try it in _City of Heroes_.

The results were disappointing. While technically playable, the game was running around 10-15 FPS in populated areas, 20-25 in instanced missions.
Something was seriously off, as this computer should be more than adequate based on the recommended specs. I checked performance, and it seemed
the CPU was pegged at 100% while the GPU was barely utilized. A 2004-era Pentium 4 just wasn't going to cut it.

## New target: 2008
This was the genesis of moving the needle from 2004 to 2008. Before deciding what to do next, I tried to track down if the system requirements
had changed much since launch. I got several different results based on source, but
[this one seems the most trustworthy](https://paragonwiki.com/wiki/The_Players%27_Guide_to_the_Cities/System_Requirements), dating from around
2010:

* **CPU:** Intel® or AMD Dual Core processor 
* **CPU SPEED:** (not listed)
* **RAM:** 2 GB
* **OS:** Microsoft® Windows® XP/Vista/7
* **VIDEO CARD:** NVIDIA® GeForce FX 5600 Series (or higher), ATI™ Radeon® 9600 (or higher) 

The new specs seem very comparable to the launch recommendation. A decent bit more memory, but I was already above that. The big thing
that tipped me off was the shift to a "dual core" processor. I was starting to realize that _City of Heroes_ is a very CPU-hungry game. In order
to target the new CPU requirements, I would have to grab a whole new Optiplex base, unfortunately, as there weren't any dual core CPU upgrades
that would work in the GX620 motherboard. Luckily all of my other parts would still be usable, so I wasn't too bummed out about this setback.

Dual core systems are quite a bit more expensive than the Pentium 4 I originally bought, so it took a more thorough scouring of
various storefronts on the web to find one at a suitable price point. I settled on an Optiplex 760 that contained a Core 2 Duo 1.8GHz, 4 GB of RAM,
and a 160 GB HDD. It cost $125, but at least it came with free shipping this time. Interestingly, when I checked the configuration via the
service tag, it originally came with an E7500 3.0GHz processor, so that had been replaced at some point with the E6300. No big deal though.
The Optiplex 760 model was from 2008, so that decided my new overall target year for the system.

<figure class="center">
<a href="/assets/images/2019/07-14/retropc-mark-2.jpg" target="_blank"><img src="/assets/images/2019/07-14/retropc-mark-2.jpg" width="512" height="384" alt="A Dell Optiplex 760 tower computer with monitor, keyboard, and mouse. The Windows Vista desktop appears on the monitor."></a>
<figcaption>Mark II of the retro PC, running Windows Vista Ultimate.<br>Sorry my computer desk is not lit very well.</figcaption>
</figure>

Of course, I stole all the nice pieces from the previous computer for the new one. So the end result is I have an 80 GB primary SSD
with a 160 GB secondary HDD as well as 8 GB of total RAM. Because of the new RAM, I decided I wanted to install a legit 64-bit operating system.
A target year of 2008 meant Windows Vista would be appropriate. Of course, I had heard all sorts of bad things about Vista at the time, so I had
never used it, skipping straight from XP to 7 at home. At work back then, we wrote code on Windows Server 2003 so I never saw it there either. So,
out of morbid curiosity, I decided to get Vista.

At this point, I got _very_ cautious about buying an old copy of Windows. There's a ton of bootleg copies to buy on eBay, as well as listed "used"
copies that made no guarantee that the included keys were still good. I did eventually find a listing that the seller provided a guarantee on. It
was quite a it more expensive than all the other listings, but I wanted to make sure I was buying a legit copy.

My copy of Windows Vista arrived, and I installed it and activated it without issue. Most of the bad criticisms came from the launch of Vista, and
it seems over the course of SP 1 and SP 2, it became fairly indistinguishable from Windows 7 from the user's perspective. The UAC is still a bit naggy,
but it's not really that bad. Getting everything up to date was even more painful than XP, though. There were a lot more updates to ddoownload, and
sometimes Windows Update would just randomly hanging, necessitating some additional research and manual installation of a few updates.

Now, _now_, I could finally give _City of Heroes_ another go. I crossed my fingers and gave it a try. Aha! Now we're finally getting somewhere. I
was able to get a reasonable 15-25 FPS in the most crowded parts of Atlas Park and between 40-60 FPS in missions. This was actually playable. I'm
actually playing with most of the settings on "high," with only a few of the more expensive options like FSAA, reflections, shadows, and ambient occlusion
turned down/off.

<figure class="center">
<a href="/assets/images/2019/07-14/coh-screenshot-2.jpg" target="_blank"><img src="/assets/images/2019/07-14/coh-screenshot-2.jpg" width="640" height="512" alt="A screenshot of City of Heroes from over the shoulder of a female super hero. The view is of a city scape with tall buildings. The super hero is wearing white and orange tights with an oragne cape and floating slightly above the ground."></a>
<figcaption>A beautiful view upon Atlas Park.</figcaption>
</figure>

## Final verdict
Even with the trials of putting the machine together and wasted time and money, I still had a lot of fun with this build. To be honest, it would
have been boring if it just worked on the first try. I think calling 2008 "retro" at this point is probably pushing the definition a bit, but then
again that's like 50 years old in computer years with how fast technology moves.

I'm still on the lookout for a period-correct graphics card, so the rig isn't completely done yet. The Radeon HD 6670 works just fine, but it's
well outside the 2008 target year. Downgrading this part by 3 years I don't think will actually affect performance much, though. As I noted,
_City of Heroes_ seems to be very much CPU-bound.

I'd also like to find some more games from that era to play on the system. _Halo 2_ seems like it would be a good candidate to see what the system
is capable of in an FPS, but people on eBay want way too much for it. I'll probably check some local thrift shops; they usually have tons of left
over old PC games as most people are loooking for vintage console games.

Here's the final parts tally and prices, minus all the stuff I eventually discarded. The only parts I already had that I used in the build were
the keyboard and mouse.

| Part | Mfr. Date | Price Paid |
| ---- | ---- | ---- |
| Dell Optiplex 760 | Late 2008* | $125 |
| Add'l 4 GB PC2-6400 RAM | Nov 2011 | $11 |
| AMD Radeon HD 6670 | Apr 2011 | $40 |
| Sound Blaster Audigy 2 ZS | Sep 2003 | $23 |
| ARCTIC MX-4 Thermal Paste** | 2019 | $7 |
| Dell P1913SF 19" LCD Monitor | Mar 2013 | $39 |
| Windows Vista Ultimate | Nov 2006 | $40 |
| | **Total** | $285 |

<small>*This particular model started shipping in late 2008, though my specific computer was built in December 2009.<br>
** Side note, I _highly_ recommend replacing the thermal paste in any old PC you get. When I took the CPU cooler off on both Optiplexes, the original
thermal paste was basically dust.</small>

<figure class="center">
<a href="/assets/images/2019/07-14/retropc-mark-2-guts.jpg" target="_blank"><img src="/assets/images/2019/07-14/retropc-mark-2-guts.jpg" width="512" height="517" alt="A Dell Optiplex 760 tower computer, laying on its side with the side panel removed. The motherboard and several components are visible."></a>
<figcaption>Mark II of the retro PC, view from the inside. For a proprietary machine,<br>the internals are surprisingly open and easy to work with.</figcaption>
</figure>
