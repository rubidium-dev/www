---
layout: post
title: City of Heroes and the private server kerfuffle, or why intellectual property
  is stupid and broken
date: 2019-04-21 06:52 -0500
card: /assets/images/2019/04-21/coh-holdtorch.jpg
tags: ["city of heroes","copyright","disney","drama","games","intellectual property","law","mmorpg","ncsoft","politics"]
---
I'm a patron and reader of [Massively Overpowered](https://massivelyop.com/) (a news site about MMORPGs), so when news broke about
[a private _City of Heroes_ server that had been running in secret for years](https://massivelyop.com/2019/04/15/score-city-of-heroes-emulator-leak/),
I just had to get my popcorn and dive into this. It intersects neatly into a couple of things I'm personally interested in: the history
of video games (and its industry) and drama, both of which were unfolding before me in real time. I won't lie, I'm a bit of a drama queen myself so I had
to dive down this rabbit hole and see how far it went. If you're not caught up on the particular story, it's okay (and you might
want to save yourself the time by not getting into it). Instead, I would like to talk about something germane: intellectual property.

(Quick aside: I am **not** a lawyer, so don't take anything in this article as anything remotely like legal advice. I'm just a person who deals
with IP on a daily basis, sees its effects on the consumer, and wants to have an opinion about it. Chances are my only-slightly-better-than-layman's
understanding will cause me to make some dumb statements below.)

_City of Heroes_ was a beloved superhero MMORPG that operated from 2004 to 2012. I say "beloved" in this case, because even seven years after its
demise, there are [dedicated communities](https://www.cohtitan.com/forum/) [still talking about it](https://www.reddit.com/r/Cityofheroes/), it
launched [at least one attempt from fans to buy the IP after it shuttered](https://www.cohtitan.com/forum/index.php?topic=10284.0), and it inspired a
[whole](https://cityoftitans.com/) [bunch](https://valiance.shogn.net/) of [spiritual](https://www.shipofheroes.com/)
[successors](http://www.heroes-and-villains.com/phpb/home.php). I didn't play much when it was live, just a few months when it first released and
also a bit when the _City of Villains_ companion/expansion was released. I do remember it fondly, though, since at the time it was one of the only
MMORPGs out there that wasn't doing yet another generic fantasy setting. At any rate, it's obvious from the above that the community around
this game has been pining for it ever since it shut down, in a way I've not really seen from most other properties. Thus, when it came to light
that a certain subsection of said community had had access to a private server for years and weren't sharing with the rest of the class, it
should be no surprise that a monumental amount of outrage ensued. But how did we get to this spectacular blowout in the first place?

<!--more-->
We got here because of intellectual property and the broken system of copyrights in the modern world (a lot of which have been influenced by U.S.
copyright laws, even if you don't live there). In a good, pure, righteous world, the moment it was apparent that the _City of Heroes_ overlords, publisher
NCSoft, were not interested in continuing to operate the game, this would be where generous community members could have taken the burden on
themselves to, if not continue to develop it, at least continue to run the servers in some fashion so people could play. Instead, what happened in
the real world is NCSoft took their toys and went home, leaving thousands of fans stranded without the game they loved. It's no wonder that
some of them would resort to extralegal means to try and keep their game of choice alive.

Taking hold of the game's assets and running servers at the very least runs afoul of copyright law (there's other things to consider, like trademark,
but I'll leave that aside for now). What's interesting about modern U.S. copyright law are two really ridiculous points: one, you don't even have
to _want_ your work to be copyrighted, it just _is_ the moment you create it; and two, it lasts for **95 years** or **author's life plus 70 years** (if
the Copyright Office can determine the author). Contrast to how it started back in the 18th century: originally you got a nice 14 years, plus another 14 years
if you cared to renew. That last part is important—based on current behavior, I'm pretty sure NCSoft may not have cared to renew. They really haven't
done anything with _City of Heroes_ since it shutdown. (I'm not going to count the appearance of certain characters in _Master x Master_ because
that seems to have been [the work of a driven fan on the dev team](https://lorehound.com/news/exclusive-ncsoft-responds-to-criticism-of-statesmans-return/).
Without him, it likely wouldn't have happened.) That means most likely the copyright for _City of Heroes_ would have expired around 2018, and the
fans just could've waited it out if nothing else. Instead, under current law they have to wait until 2099, at which point I'm pretty sure every person who
cares will be long gone. Plus, we'll all be using some sort of quantum crystal computer implanted in our skulls which probably won't even be able
to run a game designed for a PC in 2004. (Then again, lots of people, myself included, like writing emulators. So who knows?)

The automatic opt-in of modern copyright is also bizarre. I run into this myself, as I like to write code and publish it on places like
[GitHub](https://github.com/) and [GitLab](https://gitlab.com/rubidium-dev). Ninety-nine percent of the time, I honestly don't care what people
do with said code. Copy it, modify it, make money from it, go wild. But you can't just say something is "public domain" these days, because the
moment you create it, it's copyrighted. So I have to go out of my way to include a bunch of legalese from the
[CC0 license](https://creativecommons.org/share-your-work/public-domain/cc0/) to explicitly give away my rights. People create so
much junk every day, a lot of which isn't worth copyrighting in the first place. As an author, to make sure people are in the clear to use _my_ creations,
I have to put _extra special effort_ to legally protect them. **That is stupid and broken.** I should at the very least have
to state _intent_ to copyright. Personally I think there should at minimum be a process requiring registration of copyrights so it isn't
trivial to copyright every random thing that comes from people's brains.

Now, the more insidious part is the absolutely insanity of modern day copyright terms. Just take a look below of how they've inflated over
the evolution of copyright law in the U.S.

<figure class="center">
<img src="/assets/images/2019/04-21/copyright-terms-over-time.png" width="606" height="375" alt="Chart illustrating growth of copyright terms with each new legal act">
<figcaption>I'm legally required to tell you that this image is licensed <a href="https://creativecommons.org/licenses/by-sa/3.0">CC BY-SA 3.0</a> from <a href="http://www.tomwbell.com/writings/(C)_Term.html">Tom W. Bell</a>.</figcaption>
</figure>

**This is also stupid and broken.** It's pretty insane to think that in this day and age, when new intellectual property is created millions of times per
day, and the internet is awash in various creative works that get forgotten and abandoned, and trends cycle in and out almost as fast, that instead of
shortening copyright terms, we've instead extended them to insane limits that don't serve anything but corporate interests on milking the consumer for
more money from the "back catalog." Full disclosure: I work in the games industry, my livelihood depends on "intellectual property" being a thing, but
even I recognize that this level of protection is wholly unnecessary. I'm not going to get into _how_ we got here, there's already far more
[detailed](https://www.huffpost.com/entry/unconstitutionally-long-c_b_5275603) [analyses](https://repository.uwyo.edu/cgi/viewcontent.cgi?article=1020&context=honors_theses_17-18)
of that elsewhere. There is some bitter irony that one of the big drivers behind these extensions is Disney, who profits off of remixing stories that
are in the public domain.

The increasingly digital world, with subscription video services and games-as-service becoming the new norm, makes these copyright terms even more
onerous. When I play an MMORPG like _City of Heroes_, if the rightsholder suddenly decides they no longer want to operate it, I as a consumer have zero
recourse. The publisher doesn't want to run the service anymore, but as the player I am forbidden by law to run it myself. (That's even assuming I have
the technical know-how to do so.) This is the worst case scenario. At least in the past, once I bought a book or a movie or a game, I had a reasonable
expectation of being able to enjoy it, so long as my copy didn't get damaged (and I could maintain whatever devices were necessary, such as an older
console). And if it did I had some ability to get a "new" one on the second-hand market. Not perfect, but it was workable. That's no longer an option
without committing piracy.

Don't get me wrong, _copyright is a good thing_, taken at face value. There's a reason it was codified into the very Constitution of the United States
even back in the eighteenth century, long before anyone envisioned a world where everyone and their dog would be a content creator. I think people should
be able to make a living off of the non-tangible goods they create. But to be honest, that _at most_ warrants the original 14-year copyright term that
we had at the outset. At that point, most of the world has moved on. And if your work is so great that it does outlive that 14-year window? Maybe it's time
to realize that it _should_ be in the hands of the public, as it's clearly gained a life beyond the original author. There are other ways
to protect your characters and your worlds, like trademarks as I mentioned earlier. Perhaps there should be a clearer notion of the protection of
"derivative works" so that franchises can still be built, but even that might not be necessary. I know lots of people are more interested in what the
original author has to say about their extended worlds and what's "canon" as opposed to fandom creations, but fandom creations definitely _should not_
be illegal in any way, even if they do compete with your product. Especially a decade after you've had your time in the spotlight.

In all of this though, I'm less concerned about the dragons who
[jealously hoard their IP like Nintendo](https://www.polygon.com/2018/11/15/18097081/nintendo-rom-lawsuit-loveroms-loveretro-emuparadise). At least
it tells me that if they're spending money on lawyers to defend their IP, that _maybe_ they at least intend to do something with it. No, the people
I feel for are those like the _City of Heroes_ community—abandoned by an owner who no longer cares at all. An indifferent creator who's granted a century of
protection on something they don't even want, and that they would literally have to exert effort and money just to give it back to the fans. These
fans, despite their love for the IP, will be left trying to breathe life into a dead husk, wondering if at some random point in the future, they'll
get a C&D simply for loving something somebody else created. If this particular sword of Damocles hadn't been hanging over their collective heads the
whole time, I have no doubts that we would have dozens of private servers operating out in the open, and none of this week's particular nonsense would
have been necessary in the first place.