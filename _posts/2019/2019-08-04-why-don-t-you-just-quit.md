---
layout: post
title: Why don't you just quit?
date: 2019-08-04 05:02 -0500
card: /assets/images/2019/08-04/guess-ill-die.jpg
tags: ["game dev","labor","politics","privilege"]
---
It can be tempting to simplify the woes of the games industry into "obvious" solutions. "Why don't you just quit?" is one I hear a lot, many
times over the years, and it came up yet again earlier this week. In one of the industry forums that I participate in, we were discussing how
studios frequently exploit their QA staff. One person put forth an argument that boiled down to: if they just quit their jobs and go somewhere
else, the studios would have to improve because they would be unable to hire people to exploit.

Unfortunately, it's just not that simple.

<!--more-->
I found this argument particularly problematic, because it is indicative of privileged thinking and a gross over-simplification of the issue. There are a
multitude of reasons why an individual may be unable or unwilling to quit a job, and trying to apply a blanket solution like this to each person's
situation is unhelpful. Let's break it down.

## First, let's discuss privilege
I want to tackle the term _privilege_ specifically because I used it in my counter-argument to this statement. I was not surprised when they took offense
to this characterization, as most people don't like the very idea of the word and that it might be applied to them. But you have to understand that all of
us enjoy some privileges or another, and that they are not inherently good or bad. They just _are_. What's important is that we are self-aware of our
statements and try our best to empathize with those that may not have the same privileges we do.

This person in particular countered with the statement, "I grew up poor," so I knew almost immediately that they misunderstood what was privileged about
their argument. Background can definitely impact what privileges you have, but they can also be gained later in life due to changes in your personal
situation or even society as a whole. Saying "I grew up poor" as a countermand is not unlike saying, "What I said isn't homophobic, I have gay friends."
There isn't any correlation between those two things. A person who grew up (or is currently) poor can and probably does have some privileges, even if
they're not financial in nature.

Privilege is best illustrated with an example, and here's an insidiously simple but ubiquitous one. When you buy band-aids at the store, are you easily able to
find ones that match your skintone? If you answered "yes," then that's a privilege you have, because your skin color is one that society considers
"default." People who aren't affected by this issue would even say it's trivial. I mean, why should buying band-aids that match your skin color matter?
For people whom this _does_ affect, though, it's just one more way in which it's obvious that they aren't the norm, a small nuisance in a sea of them that
piles up over time.

Here's another example that affects me personally. When you go out in public with your significant other, do you think twice before holding their hand?
I certainly do. Despite mostly feeling safe in a country with progressive-trending views on LGBT people, I still know that there are times when I should
not hold my wife's hand or make any public display of affection for fear of physical harm. Heterosexual couples simply don't have to worry about this,
and thus that is a privilege they enjoy that I do not.

Bringing it back to the statement about quitting one's job, why is this a privileged statement? Because the person who stated it was projecting their own
situation on other people. Because they personally had the means to quit a problematic job on principle, they simply assumed that _everyone_ should be able
to do that same thing, and they would not accept the argument that some folks simply do not have the security necessary to do something like that.

## Living in captivity
The games industry in particular is really good at holding their employees captive. Just getting into the industry often means taking an entry-point job
like QA to get a foot in the door, and the studios know this and use it to their advantage. They love to dangle the prospect of getting a better role
or full-time employment in front of these folks to get them to put in extraordinary effort, and this amount of effort has adverse effects on health and
other aspects of the person's life.

> Isaac Torres, a former QA analyst for NetherRealm, told Variety that he worked 90 to 100 hours per week for a period of four months. He explained that
> the pressure to work overtime came partly from the desire to get a full-time position with the company. "At the time [that I worked at NetherRealm],
> full-time positions were seen as this coveted thing, so there was always constant pressure to one-up everyone so you're noticed," Torres wrote.
>
> <small>—['Mortal Kombat 11' Game Devs Allege Toxic Work Conditions at Warner Bros. Interactive's NetherRealm](https://variety.com/2019/gaming/features/netherrealm-studio-warner-bros-games-toxic-1203204728/)</small>

These problems are multi-faceted. Studios already exploit the fact that many passionate people are competing for a small number of available
positions. If someone were to quit their job at a studio, especially for a position considered "unskilled" by many studio leaders, that probably
means exiting the games industry itself. Additionally, if they quit in the middle of a crunch, they likely won't get any positive references
to other positions.

On top of that, if someone is working 90 to 100 hours per week, how are they supposed to find the time to seek new employment? Many QA positions are
not paid a livable wage, so analysts are living paycheck-to-paycheck. They can't afford to quit the existing job before finding a new one, and there's
not enough hours in the day to job hunt (let alone compounding the stress and mental exhaustion of a demanding job with a search for a new role). And
if they need to take a day or even just an afternoon off for an interview, they're likely going to have to take that time off unpaid, if it's even
granted. QA personnel rarely get paid time off except for a few precious sick days.

Speaking of sick days, many QA come into the office sick because they knew if they didn't, they'd be seen as less valuable than their peers. I've witnessed
this phenomenon multiple times first-hand. So even if they have paid time off for sick leave, they often don't take advantage of it.

Also, don't forget an individual's situation. If they're lucky enough to get health benefits, they may depend on those to a degree that makes moving
jobs difficult, especially since so few low-level and contract positions offer them. They might be caring for children or elderly at home that are
dependent on their income or benefits. They might be an immigrant, which limits their job mobility.

The more you take the time to see it from each individual's perspective, the more it should be obvious that "Why don't you just quit?" isn't a
one-size-fits-all solution. There's a lot of nuance to pay attention to, and it's disrespectful to suggest that what works for one person will work
for everyone. Certainly there are people who quit their abusive employers, and some even manage to do so without giving up their dream of working
in the games industry. But there are always exceptions.

You must also realize that positions like QA, customer service, entry-level artists and programmers, all have insanely high competition for those spots.
The industry practically views them as a commodity, so quitting on principle won't have much impact on the studios' behavior. They'll just find more
warm bodies they can wring through the grinder and then replace.

But perhaps worst of all, telling someone to quit is basically blaming the victim in this situation. These fine people shouldn't _have_ to quit to
get better hours, better pay, and a more humane work environment. It is entirely the fault of the studios that this situation exists; it is the studios
who collectively exploit their workers that should be required to do something different, not the individuals who are exploited.

## So, what _can_ be done?
None of this is to say we should give up trying! Articles like the one I linked above are shared with more and more frequency, shedding light on a
problem that used to be more of an open secret. Fewer people at all levels of the industry are accepting this situation going forward. While there will
never be enough of a shortage of entry-level talent to hurt a studio's bottom line, seasoned professionals like myself _can_ make a difference.
I make it a point when I interview with a new studio to discuss work/life balance. As a leader, I make sure to impress on my staff and other
leaders its importance as well. If the supply of senior-level talent willing to put up with crunch culture and toxic workplaces starts to dry
up, then the industry _will_ start to shift, and it has. Slowly, we are arcing towards the right path to a sustainable future.

Sadly, the mindset behind abusive workplaces is deeply entrenched, and it may still take another decade or two to weed it out. I would love to
see game devs unionize to protect themselves from poor workplace policies (that is exactly what unions were meant to solve, after all), but
conservative/corporate politics and media have so successfully stigmatized that word in modern society that I doubt it will ever come to pass.

Instead, we will have to rely on those who do have the power to make a change. This is something that will have to be solved top-down, not by
laying it at the feet of the people who are at the bottom.