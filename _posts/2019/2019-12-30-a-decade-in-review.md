---
layout: post
title: A decade in review
date: 2019-12-30 10:27 -0500
tags: ["personal"]
---
As 2020 and a new decade dawns, I thought it would be nice to go back in time for a bit and reminisce about how the 2010s went and also to talk
about where I'm at today. Just thinking about it, I'm surprised about how much my life changed in the past 10 years.

Personally, I probably couldn't be doing better. I have a loving, stable marriage. We have enough money that we don't have to worry about the
unexpected too much. Since I started taking meds for my [generalized anxiety disorder](https://adaa.org/understanding-anxiety/generalized-anxiety-disorder-gad) a
few years ago, my quality of life has improved drastically. I don't smoke anymore, and I barely drink alcohol (which is far cry from a few years ago,
more on that later).

<figure class="center">
<img src="/assets/images/2019/12-30/goingforme.jpg" width="600" height="450" alt="(meme) So I've got that going for me. Which is nice.">
</figure>

Career-wise, it's a different story. I'm fast approaching the quarter-century mark in my career, and I find myself facing a crossroads. For the past ten
years of my life, I've worked in the games industry, which is notoriously rough on people. I'm reaching an age where I can't work like I used to, putting
extra hours and dealing with constant chaos. Work-related stress is a constant factor that affects my mental and physical health. As much as I love what
I do, I'm not sure it's good for me. I think one of the things that's reinforced that thought lately is that I've taken my first large chunk of time off
this holiday season in a long time, and I find myself sleeping better and not leaning so heavily on my anxiety meds. That said, I'm not certain I could
go back to working in business software or something like that, despite the stable hours and better paychecks.

A few months ago, I considered relocating to Sweden for a new job (still games). It was enticing, I must say. I've always considered living and working
abroad at some point (I live in the United States). I don't think it was quite the right opportunity for me, though, and I passed on it. Even now, I
sometimes wonder if I made the right decision, or if I will end up regretting it.

So, that's where I'm at today. Let's look at the past decade, year by year.

<!--more-->
## 2010
I remember this year well. January 1 was the day my wife finally moved out. It wasn't a surprise, but it still hurt. We'd had our ups and downs for the
last two or three years, and I had been self-medicating with alcohol already. The alcohol abuse got much worse after that. I didn't cope well with being
alone again.

Despite all that, I somehow managed to keep my career on track (I'm really good at faking normal functionality at work when I need to). This was also the
year I moved from boring corporate jobs at various technology companies to working in video games. At the time, I was a platform software engineer, so I
started out working in payment systems and digital distribution. I still work on platform support to this day... after working on a couple titles directly
over the years, I've found I actually like the platform work more than building the games themselves.

## 2011
I honestly don't remember much about this year. Maybe it was the alcohol? I was kind of in stasis for a while, not really sure what I wanted to do with
my life. Two significant things did happen in 2011, however.

First, I finally hit a breaking point with the alcohol abuse. At a friend's birthday party, I drank ... _a lot_. I'm not even sure how much. I do remember
that I blacked out early in the evening, and at some point some good friends carried me back home and made sure I didn't die. It was bad enough that I
called out sick from work for three days. This ended up as a turning point for me, however. After that, I decided I was done. I dumped all the alcohol I
had in the house in a symbolic move and quit drinking as much as I could, which was hard. My willpower didn't completely hold at times, but things got a
lot better. I also bought my friends some fancy kitchen knives as a thank you for not letting me drink myself to death.

After that, I reconnected with religion and faith. I'm not saying I went the cliche route of "born-again" after quitting alcohol, but an interesting thing
happened after I stopped drinking. I started to forgive myself for a great many things and move on with my life. I started to like myself again. And through
finding love for myself, I accepted God into my life once again. That was a deeply personal journey, so that's all I'll say about it here.

## 2012
This year was a year of healing. I started dating again, with the actual intention of forming relationships. (The past couple of years was mostly about
quick hook-ups and other fairly unhealthy relationship behavior.) Mostly out of laziness, neither my wife nor I had actually gotten around to formally
divorcing, so I took care of that.

Most importantly about 2012, though, is this was the year I met my second (and current) wife. We met on a dating web site, so those do work! (Or at
least they did, before they became nothing but bots.)

## 2013
I proposed to my now-wife in the summer, and obviously she said yes. Later on I changed jobs and moved to southern California from
Texas. That was a bit of a culture shock. I don't think I ever adjusted to all the palm trees and the summer-like weather in the winter months.

## 2014
This was the year I actually got married. We had a beautiful fall wedding at my in-laws house (they have land in rural Texas). I think it's the only time
both our families and our friends have all been in the same place. We went to Disney World for our honeymoon, because we're a couple of big kids.

## 2015
I finally managed to quit smoking, dropping a nearly twenty-year habit. This was a lot harder for me than quitting alcohol for some reason, maybe because it
was a more long-term and daily habit. I tried several times in past years only to relapse. I think the thing that finally cinched it for me was vaping. I was
able to use vape cartridges to stage down the amount of nicotine gradually, and that worked better than any other treatment I'd tried.

Also, I finally got tired of southern California. I always loved the Pacific Northwest, so we decided to give it a shot and moved to Seattle. I was actually
surprised how quickly the job offers rolled in; apparently there's a lot of competition for talent there. Unfortunately because of this I think
I made some bad choices. At first I just took the one that offered the most money (lesson learned—never do this), and the studio wasn't a great cultural fit.
The project was interesting, but I just didn't fit in with the people. I left that job after only a few months, and next I ended up at a small studio that was a
much better fit for me. That didn't go so well, either, though—they ran out of money and shut down less than a year later without releasing any games.

## 2016
After two false starts, I was feeling homesick. Midway through 2016, we moved back to Texas, and we've been home ever since. It's nice being closer
to old friends and family, and everything feels a bit more familiar than other parts of the U.S., despite not living in the area where I grew up. I
also found a pretty good job that's more stable than the last few gigs, and I still work there today.

## 2017, 2018, 2019
To be honest, the last few years kind of blend together. I've been living in the same place and working at the same job. There's
a nice certainty to it, if a bit boring at times. Since joining my current studio, I've been promoted to a director-level position, which is great for
my paycheck but weird for me personally. I don't write code myself much anymore, and I feel my skills atrophying. Sometimes I long for the simpler days
of just writing code all day. I've tried to start a few side projects at home to keep my skills sharp, but I find it difficult sometimes to fit in time
for several hours of coding a week after work, especially since it frequently just feels like more work instead of recreation.

One weird thing that happened in late 2018 and into 2019 is I started feeling my [gender dysphoria](https://www.psychiatry.org/patients-families/gender-dysphoria/what-is-gender-dysphoria)
heavily again and having self-esteem and body image issues. I'm not sure where it came from, but it's persisted for a while. I transitioned many years
ago (long enough that you'll note it's not on this decade-in-review), but I never went as far as getting
[gender confirmation surgery (GCS)](https://www.healthline.com/health/transgender/gender-confirmation-surgery). After months of consideration, this
summer I underwent a vaginoplasty. I touched on this subject a little in [my previous post about redesigning my fursona](/2019/05/18/ruby-redesigning-a-fursona.html).

It's improved my mood considerably. Despite identifying as mostly asexual these days, just the improvement to my self-image
has helped my mental health. I may explore the topic of my self-esteem and self-image in the future; it's something that's on my mind a lot lately.

My wife and I keep ourselves busy by trying out restaurants, building with LEGOs, and playing video games and tabletop RPGs. It's a simple existence,
but I enjoy it. As I mentioned earlier, we have enough money we don't have to worry about most things. My biggest worry right now is what to do about
my career going forward. I don't want the games industry to burn me out completely.

Let's see what 2020 holds.

<figure class="center">
<img src="/assets/images/2019/12-30/leo-cheers.jpg" width="600" height="400" alt="(meme) Leonardo DiCaprio holding a glass of champagne.">
</figure>
