---
layout: post
title: The sound of water
date: 2019-06-14 09:44 -0500
card: /assets/images/2019/06-14/angelfish.jpg
tags: ["home devices","memories","personal","zen"]
---
The past week or so, I've been stuck at home recovering from surgery. One of the features I've discovered that I really like on
my Google Home is that I can tell it to "play nature sounds." These are pretty relaxing on their own, and contain some of the
typical things like babbling brooks and thunderstorms. What I wasn't expecting was that these sounds would trigger a memory
for me.

When I was really young (like 5 or 6?), I used to spend time at my cousins' house every once in a while, playing with them and
having sleepovers. They were close to me in age, so we got along really well. It was a welcome reprieve, since at home my brother
was a good six years older than me, and he mostly tried to pretend I didn't exist (except for the times when he bullied me instead).
My uncle had an aquarium in his living room. I don't really remember much about it or even what kind of fish were in it. But I
do remember distinctly the sound the water pump made, the constant trickle of water cycling in the glass tank.

<figure class="center">
<img src="/assets/images/2019/06-14/fishtank.jpg" width="600" height="450" alt="Octagonal fish tank with a wood stand">
<figcaption>I think it looked a little like this?</figcaption>
</figure>

One of the babbling brook sounds that my Google Home makes is nearly identical to the sound of that water pump. When I listen to it
at night while I'm trying to fall asleep, I'm taken back to that time when I was a kid, sleeping in the living room with the
aquarium as the only sound. I'm taken back to that short time when I got a trip away from my own home, away from my brother who
bullied me and my parents who were arguing all the time. It's a weird zen moment that brings me peace even now as an adult, nearly
forty years later.

It amazes me how something as simple as a sound can trigger so many memories and feelings.