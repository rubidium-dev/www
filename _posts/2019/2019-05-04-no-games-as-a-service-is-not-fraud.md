---
layout: post
title: No, games-as-a-service is not fraud
date: 2019-05-04 11:40 -0500
card: /assets/images/2019/05-04/ace-attorney.jpg
tags: ["games","intellectual property","law","mmorpg"]
---
... but you might be upset about them, and you don't want to buy them. That's okay.

A video on this subject from YouTube channel Accursed Farms (creator Ross Scott) has been passed around my social circles the past few days. I'll
admit, it's over an hour long so I didn't watch it the whole way through, but I did skim through the highlights and participate in the discussion.
The very title of the video, _"Games as a service" is fraud_, already starts off on a false premise (fraud has a very specific legal definition),
but I suppose "games as a service is morally objectionable" wouldn't be nearly as eye-catching a title.

{% youtube tUAX0gnZ3Nw %}

As I mentioned above, I'm not going to argue that you shouldn't be upset about games-as-a-service (you're welcome to your own feelings and purchase
preferences), but I do want to talk about why it's not a fraudulent business practice.
<!--more-->
First, let's make sure we know what games-as-a-service means in this context. A game-as-a-service typically:
* Is continually or periodically updated after launch (i.e. the product you bought isn't its final form).
* Includes some sort of monetization, whether that's subscriptions or microtransactions or similar.
* Will eventually end that service (i.e. its dependent on an online portion that can be revoked by the developer/publisher).

This is a shift in the business model of games that's happened gradually over the past decade or so. Previously, you bought a cartridge or a disc of some
sort, and you could play it as much as you want, with no material changes, and no additional money spent. Games-as-a-service undoubtedly represents
a shift of power away from the player to the publishers, as they have far more control over your access and experience than they used to. I won't
debate that. You can lose access to the game at any time. You might have to spend more money than when you started to get the full experience. Over
time, the game might not even be the same game you originally paid for. These things aren't necessarily what you wanted when you bought the game
at the outset. But is it fair to call it _fraudulent?_

It's difficult to talk about the word fraud in a discussion that's tied up in the emotions of the player when they feel they've been wronged. People
tend to reach for words like "fraud" or "negligence" when they feel personally wronged by a business' actions, but unfortunately for them, courts
have specific definitions around these terms that the actions of the developer or publisher would have to meet in order for you to have a legal case.
For [fraud](https://www.law.cornell.edu/wex/fraud), specifically: "deliberately deceiving someone else with the intent of causing damage." Two key words
in that sentence: _deliberate_ and _intent_.

In order to accuse a developer/publisher of deliberate deception and intentional damage, you'd have to prove that they somehow sold the game to you
with the intent to ultimately deceive you. Unfortunately, that's just not the case. I will refer you to those pesky pages-long documents you rapidly
click past every time you play a new game: the end-user license agreement (EULA). I guarantee you they have plenty of verbose legalese that has been
carefully crafted to outline exactly what the responsibilities are of each party in the transaction. It's not sexy, and no one wants to read forty
pages of legal text before playing their favorite game, but it's all there. _Technically_ you were informed, even if you chose not to read it. So
far EULAs have held up in court, and I don't see that changing any time soon. Some minor points like forced arbitration and denial of class action
have faced various challenges, but I doubt EULAs as a whole are going away—to do so would legally and financially imperil the entire software industry,
and I doubt any court would be so reckless.

This is where one of Scott's early premises falls apart almost immediately. He asserts that games are a "good," but they're not. They never have been.
Even in the days of offline-only, cartridge-based games, you didn't _own_ the game, you only were allowed specific use of your _licensed copy_. You
could play it at home as much as you want, but you couldn't make a copy of it and give it to a friend. You could resell your one copy, which has some
of the properties of being a good, but that was just an unintentional side effect of the distribution method. Publishers didn't like that (just go back
and look at [how much effort Nintendo spent trying to kill the rental industry](https://www.youtube.com/watch?v=J3xuy5YALl0)), but they had no way to
legally stop you at the time. Now they do. By purchasing/playing the game, you implicitly agreed to this relationship where you lack power over thing
you bought. You can't redefine it as fraudulent because the other party exercised their rights as part of that agreement.

Also in one of Scott's slides he includes the remark, "Only occurs because of negligent design." [Negligence](https://www.law.cornell.edu/wex/negligence)
is another of those pesky terms that has a very specific legal definition. To a layman, this statement sounds like developers somehow accidentally
stumble into designing games this way out of ignorance or incompetence. But for the game design to be negligent _legally_, there has to be some sort of
legal duty to the players not to change the game in ways they don't like or end the service before they wanted it to end. But again, the terms of
that relationship were already covered by the EULA, which I guarantee you gives the publisher and developer leeway to change the game in these ways
without the player's consent and without any guarantee that it won't happen.

No doubt, some players really _want_ games-as-a-service to be declared fraudulent, negligent, or any number of things that would make the games
industry stop building games like this. I get that. But it's irresponsible to imply that the situation is so black-and-white. Any challenge to the
business model legally would be a huge uphill battle. As a consumer, the best thing you can do is to not buy the games that you don't agree with
on a moral/ethical level. If a publisher advertises a game as having microtransactions, DLC, forced online, etc., and you don't want that, then
don't buy it. It sucks not being able to buy a game you really wanted, but you have to weigh that against whether the experience will ultimately
give you more fun or frustration. The best thing you can do as an individual is try to be an educated consumer and only buy the games that will
ultimately spark joy.

My first piece of advice? Stop pre-ordering. The bonuses are rarely worth it, and we're _mostly_ past the point of scarcity where if you don't pre-order
you won't be able to find a copy on opening week. (Somehow this is another place [Nintendo still tends to get wrong](https://arstechnica.com/gaming/2017/06/nintendo-switch-shortages-are-definitely-not-intentional/).)
If you're really concerned about the business model of a game, you may want to wait a week or a month first to see how it pans out. Personally,
I frequently will wait a while for a bundled edition to hit the shelves for a lot of games so that I can get the base game and all the DLC in one
cheaper package. It also gives me time to gather other people's feedback on the game to make sure I'm still interested in it.

One closing piece of information I want to include as well: while I firmly believe "fraud" is the wrong term to use in this situation, I don't
believe that games-as-a-service are completely in the clear. There are a lot of other legal nuances that could be examined here as far as consumer
protections go. As I was drafting this post, a friend sent me a link to another video responding to Scott's video. This one is from Leonard French,
an actual lawyer, and he examines several of the same points I do from a more experienced perspective as well as bringing up some ways that
games-as-a-service could be anti-consumer. It's a much shorter video and well worth the watch.

{% youtube ZpgT_EdtAW0 %}

You may also be interested in my article about [_City of Heroes_ private servers](/2019/04/21/city-of-heroes-and-the-private-server-kerfuffle-or-why-intellectual-property-is-stupid-and-broken.html),
which talks a lot about copyright and intellectual property. It's the basis of why you can't do what you want with your games in the first place.