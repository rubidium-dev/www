---
title: Trans day of visibility
date: 2019-03-31 14:04 -0500
tags: ["lgbt","personal","trans"]
---
I'm not sure if it's a subconscious thing or just a conincidence, but I seem to have this habit of starting new blogs on or around
significant dates for the LGBT community. My last two blogs were started on [National Coming Out Day](https://en.wikipedia.org/wiki/National_Coming_Out_Day)
(in October). Today's significant date is the [International Transgender Day of Visibility](https://en.wikipedia.org/wiki/International_Transgender_Day_of_Visibility).

With a lot of trepidation, I wrote this first post on a previous blog back in 2011:

<!--more-->
> I am transsexual. To be explicit, when I was born my parents raised me to be a gender I'm not, and it caused me a lot of pain,
trying to figure out why I felt so wrong. I can't really fault them; most people's understanding of gender boils down to what's between the
legs. Unfortunately it's so much more complex than that, and for the average person it's difficult to understand why your little baby boy
likes _My Little Pony_ better than _G.I. Joe_ and steals mom's makeup while she's not looking. Gender and sexuality are both such fluid
things that most people try to divide into neat little categories. Thankfully, after twenty-six years of trying to fulfill a role I didn't
want or understand, I finally found the strength to be who I really am, and there's nothing in this world that could convince me to deny
myself ever again.

This still holds true today. When I first decided to transition from male presentation to the female truth back in 2006, I really wanted
to stay "stealth." I just wanted people to accept me as a woman, no questions asked. Over time, though, I realized the invisibility wasn't
true to who I am as a person. Even at that point, though, it took me over five years to realize that I needed to become visible, to emerge
from the shadows I hid in.

Since then, I've been very open about my trans status. Not just on the internet, but with family, friends, coworkers... I didn't want to
be invisible anymore. I wanted people to know I'm trans, because trans people exist and are real. People need to know that we are far
more common than they think, and we're not going anywhere.

I remember a couple of years ago, as an introduction to a new managerial group at work, I gave a [pecha kucha](https://www.pechakucha.com/)-style
presentation about my life and career. In that meeting, I told about twenty people whom I really didn't know yet my life story, including
the fact that I'm trans. They still applauded at the end, and they treated me like any other person from that day forward. That was what I
wanted—not to be hidden and accepted, but to be visible and accepted. On top of that, after I gave that presentation, another woman who was
in the room contacted me privately to thank me. She too was trans, but she didn't feel like she had the courage to be as public about it as
I was. I told her that that's okay. Living a life out and visible is sometimes scary, and no one should feel pressured to do so unless they
are ready.

But for me, personally, I'm here. I'm out. I'm visible. Happy transgender day of visibility!