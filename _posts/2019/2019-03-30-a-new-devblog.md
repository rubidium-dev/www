---
title:  "A new devblog"
date:   2019-03-30 13:06:46 -0500
tags: ["blog","games","personal"]
---
Brand new web site! I'm not 100% sure what all I will use this space for just yet, but chances are some of it will be programming related,
some of it will be furry stuff, maybe even some introspective real-life thoughts.
